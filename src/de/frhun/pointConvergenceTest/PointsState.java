package de.frhun.pointConvergenceTest;

import java.util.ArrayList;
import java.util.List;

public class PointsState {
	public final ArrayList<Point> points;
	
	public PointsState(List<? extends Point> points) {
		this.points = new ArrayList<>(points.size());
		for(Point p : points) {
			this.points.add(p.clone());
		}
	}
}
