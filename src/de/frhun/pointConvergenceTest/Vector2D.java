package de.frhun.pointConvergenceTest;

public class Vector2D implements java.lang.Cloneable {
	
	protected double x, y;

	public Vector2D() {
		x = 0;
		y = 0;
	}
	
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public void setX(double d) {
		this.x = d;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	
	public Vector2D add(Vector2D toAdd) {
		x += toAdd.getX();
		y += toAdd.getY();
		return this;
	}
	
	public Vector2D diff(Vector2D target) {
		this.y -= target.getY();
		this.x -= target.getX();
		return this;
	}
	
	public Vector2D div(Vector2D toDivBy) {
		x /= toDivBy.getX();
		y /= toDivBy.getY();
		return this;
	}
	
	public Vector2D div(int toDivBy) {
		x /= toDivBy;
		y /= toDivBy;
		return this;
	}
	
	public Vector2D mul(double toMulBy) {
		x *= toMulBy;
		y *= toMulBy;
		return this;
	}
	
	public Vector2D inv() {
		x *= -1;
		y *= -1;
		return this;
	}
	
	public Vector2D inv(boolean invX, boolean invY) {
		if(invX)
			x *= -1;
		if(invY)
			y *= -1;
		return this;
	} 
	
	public Vector2D rot(double rad) {
		double x_ = x * Math.cos(rad) - y * Math.sin(rad);
		y = x * Math.sin(rad) + y * Math.cos(rad);
		x = x_;
		return this;
	}
	
	public double abs() {
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	}
	
	public Vector2D clone() {
		return new Vector2D(x, y);
	}
	
	public static Vector2D average(Vector2D... vectors) {
		int n = 0, xs = 0, ys = 0;
		for(Vector2D v : vectors) {
			n++;
			xs += v.getX();
			ys += v.getY();
		}
		return new Vector2D(xs / n, ys / n);
	}
}
