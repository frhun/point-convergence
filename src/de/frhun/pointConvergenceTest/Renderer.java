package de.frhun.pointConvergenceTest;

import java.awt.BasicStroke;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class Renderer extends Canvas {
	private final int POINT_RADIUS = 5, FORCE_VECT_WIDTH = 2;
	private double uIScale;
	private static final Color CAT_COLOR[] = {Color.black, Color.blue, Color.orange, Color.red};
	private static final Color VECT_COLOR = new Color(0xaa, 0xaa, 0xaa, 0xaa),
			CONN_COLOR = new Color(0xbb, 0x99, 0x99, 0x99);
	
	private AdvancedArranger arranger = new AdvancedArranger(); 
	private PointsState currentState;
	private boolean onTheMove = false, mousePressed = false, rotateKeyPressed = false;
	private int mousex = 0, mousey = 0;
	boolean centerOnWeight = false;
	boolean drawConnections = false;
	
	public Renderer(double scaleFactor) {
		this.uIScale = scaleFactor;
		this.setBackground(new Color(252, 244, 212));
		this.setSize(new Dimension(s(500), s(500)));
		this.addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if(mousePressed || rotateKeyPressed) {
					arranger.changeRot(e.getPreciseWheelRotation()/6);
				} else {
					arranger.zoom(0.05 * e.getPreciseWheelRotation());
				}
			}
		});
		
		this.addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				if(!onTheMove) {
					onTheMove = true;
					mousex = e.getX();
					mousey = e.getY();
				} else {
					arranger.move(new Vector2D(-(mousex - e.getX()), -(mousey - e.getY())));
					mousex = e.getX();
					mousey = e.getY();
				}
			}
		});
		this.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if(onTheMove) {
					onTheMove = false;
				}
				mousePressed = false;
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				mousePressed = true;
			}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {}
		});
		this.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {}
			
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_CONTROL) {
					rotateKeyPressed = false;
				}
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				rotateKeyPressed = e.getKeyCode() == KeyEvent.VK_CONTROL;
			}
		});
		
		Dimension size = new Dimension(s(500), s(500));
		this.setSize(size);
		this.setMinimumSize(size);
	}
	
	public void setCurrentState(PointsState currentState) {
		this.currentState = currentState;
		if(centerOnWeight) {
			int n = 0, xs = 0, ys = 0;
			for(Point p : currentState.points) {
				n++;
				xs += p.getPosition().getX();
				ys += p.getPosition().getY();
			}
			arranger.currentCenter = new Vector2D(xs / n, ys / n);
		}
	}
	
	@Override
	public boolean isLightweight() {
		return true;
	}
	
	@Override
	public void update(Graphics g) {
		paint(g);
	}
	
	@Override
	public void paint(Graphics g) {
		Image offscreen = createImage(this.getWidth(), this.getHeight());
		Graphics oG = offscreen.getGraphics();
		{
			final Graphics2D g2 = (Graphics2D) oG;
			final int r = (int)arranger.scale(POINT_RADIUS), d = 2*r;
			final int w = (int)arranger.scale(FORCE_VECT_WIDTH);
			
			if(currentState != null){
				canvasPos previous = arranger.arrange(new Vector2D());//for Chains
				for(Point p : currentState.points){
					canvasPos coord = arranger.arrange(p.getPosition());
					if(coord != null){
						g2.setColor(getCatColor(p.getCat()));
						g2.fillOval(coord.x-r, coord.y-r, d, d);
						if(p.hasForce()) {
							g2.setColor(VECT_COLOR);
							g2.setStroke(new BasicStroke(w));
							canvasPos forceEndpoint = arranger.arrange(p.getPosition().add(p.getForce()));
							g2.drawLine(coord.x, coord.y, forceEndpoint.x, forceEndpoint.y);
						}
						//Chains
						if(drawConnections) {
							g2.setColor(CONN_COLOR);
							g2.setStroke(new BasicStroke(w));
							g2.drawLine(previous.x, previous.y, coord.x, coord.y);
							previous = coord;
						}
					}
				}
			}
		}
		g.drawImage(offscreen, 0, 0, this);
	}
	
	private static Color getCatColor(int cat) {
		if(cat < CAT_COLOR.length -1)
			return CAT_COLOR[cat];
		else
			return CAT_COLOR[CAT_COLOR.length - 1];
	}
	
	private int s(int size) {
		return (int)((double)size * uIScale);
	}
	
	private double s(double size) {
		return (size * uIScale);
	}
	
	class AdvancedArranger{
		Vector2D center = new Vector2D(250, 250);
		Vector2D currentCenter = new Vector2D(25000, 25000);
		double scale = 0.01;
		double angle = 0;
		boolean invertedX = false, invertedY = false;
		
		public void setInvertedX(boolean inverted) { invertedX = inverted; Renderer.this.repaint();}
		public void setInvertedY(boolean inverted) { invertedY = inverted; Renderer.this.repaint();}
		
		public void zoom(double zoom) {
			scale *= 1.0 + zoom;
			Renderer.this.repaint();
		}
		
		public void move(Vector2D movement) {
			movement.rot(-angle).inv(invertedX, invertedY);
			currentCenter.setX(currentCenter.getX() + ((movement.getX())/(scale*uIScale)));
			currentCenter.setY(currentCenter.getY() + ((movement.getY())/(scale*uIScale)));
			Renderer.this.repaint();
		}
		
		public void changeRot(double rad) {
			angle += rad;
			Renderer.this.repaint();
		}
		
		public canvasPos arrange(Vector2D p) {
			Vector2D relPos = (angle == 0.0 ?
					currentCenter.clone().diff(p).inv(invertedX, invertedY).add(center.clone().mul(1/scale)) :
					currentCenter.clone().diff(p).inv(invertedX, invertedY).rot(angle).add(center.clone().mul(1/scale))
					);
			
			return new canvasPos(
					s(relPos.getX() * scale),
					s(relPos.getY() * scale)
					);
		}
		
		public double scale(double val) {
			return s(val);
		}
	}
	
	public AdvancedArranger getArranger() { return arranger; }
	
	private static class canvasPos{
		final int x, y;

		public canvasPos(double x, double y) {
			super();
			this.x = (int)x;
			this.y = (int)y;
		}
	}
}
