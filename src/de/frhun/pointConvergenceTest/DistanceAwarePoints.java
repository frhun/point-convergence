package de.frhun.pointConvergenceTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import de.frhun.java.util.FixedMaxFinder;

public class DistanceAwarePoints{
	final ArrayList<DistanceAwarePoint> points;
	
	public static double inaccuracy = 0;
	
	public DistanceAwarePoints(List<? extends Point> points, boolean chain) {
		super();
		Random rng = new Random();
		this.points = new ArrayList<>(points.size());
		for(Point p : points) {
			DistanceAwarePoint np = new DistanceAwarePoint();
			np.setPosition(p.getPosition());
			np.setCat(p.getCat());
			this.points.add(np);
		}
		
		if(!chain) {
			for(DistanceAwarePoint p : this.points) {
				p.setTargetDistances(new HashMap<DistanceAwarePoint, Double>(points.size()));
				p.calculateInacurateDistances(p.targetDistances, rng, 0, 0);
			}
		} else {
			{//first
				HashMap<DistanceAwarePoint, Double> distances = new HashMap<DistanceAwarePoint, Double>(2);
				distances.put(this.points.get(1), this.points.get(1).getPosition().diff(this.points.get(0).getPosition()).abs());
				distances.put(this.points.get(2), this.points.get(2).getPosition().diff(this.points.get(0).getPosition()).abs());	
				this.points.get(0).setTargetDistances(distances);
			}
			//middle
			for(int i = 1; i < this.points.size() - 2; i++) {
				calcChainDistance(this.points.get(i-1), this.points.get(i), this.points.get(i+1), this.points.get(i+2));
			}
			{//secont to last
				int baseIndex = this.points.size() - 2;
				HashMap<DistanceAwarePoint, Double> distances = new HashMap<DistanceAwarePoint, Double>(2);
				distances.put(this.points.get(baseIndex-1), this.points.get(baseIndex-1).getPosition().diff(this.points.get(baseIndex).getPosition()).abs());
				distances.put(this.points.get(baseIndex+1), this.points.get(baseIndex+1).getPosition().diff(this.points.get(baseIndex).getPosition()).abs());	
				this.points.get(baseIndex).setTargetDistances(distances);
			}
			{//last
				int baseIndex = this.points.size() - 1;
				HashMap<DistanceAwarePoint, Double> distances = new HashMap<DistanceAwarePoint, Double>(2);
				distances.put(this.points.get(baseIndex-1), this.points.get(baseIndex-1).getPosition().diff(this.points.get(baseIndex).getPosition()).abs());	
				this.points.get(baseIndex).setTargetDistances(distances);
			}
		}
	}
	
	private void calcChainDistance(DistanceAwarePoint previous, DistanceAwarePoint current, DistanceAwarePoint next, DistanceAwarePoint nextOver) {
		HashMap<DistanceAwarePoint, Double> distances = new HashMap<DistanceAwarePoint, Double>(3);
		distances.put(previous, previous.getPosition().diff(current.getPosition()).abs());
		distances.put(next, next.getPosition().diff(current.getPosition()).abs());
		distances.put(nextOver, nextOver.getPosition().diff(current.getPosition()).abs());
		current.setTargetDistances(distances);
	}
	
	public DistanceAwarePoints(List<? extends Point> points, double gate, int nearest) {
		super();
		Random rng = new Random();
		this.points = new ArrayList<>(points.size());
		for(Point p : points) {
			DistanceAwarePoint np = new DistanceAwarePoint();
			np.setPosition(p.getPosition());
			np.setCat(p.getCat());
			this.points.add(np);
		}
		for(DistanceAwarePoint p : this.points) {
			p.setTargetDistances(new HashMap<DistanceAwarePoint, Double>(points.size()));
			p.calculateInacurateDistances(p.targetDistances, rng, gate, nearest);
		}
	}

	public ArrayList<DistanceAwarePoint> getList(){ return points; }
	
	public class DistanceAwarePoint extends Point{
		HashMap<DistanceAwarePoint, Double> targetDistances;
		HashMap<DistanceAwarePoint, Double> distances;

		public DistanceAwarePoint() {
			super();
			distances = new HashMap(points.size());
		}
		
		public void setTargetDistances(HashMap<DistanceAwarePoint, Double> targetDistances) {
			this.targetDistances = targetDistances;
		}
		
		public void caclulateDistances() {
			calculateDistances(distances);
		}

		void calculateDistances(HashMap<DistanceAwarePoint, Double> target) {
			for(DistanceAwarePoint p : points) {
				if(p != this) {
					target.put(p, p.getPosition().diff(this.getPosition()).abs() );
				}
			}
		}
		
		void calculateInacurateDistances(HashMap<DistanceAwarePoint, Double> target, Random rng, double gate, int nearest) {
			Iterable<DistanceAwarePoint> toCalculate;
			if(nearest > 0) {
				FixedMaxFinder<DistanceAwarePoint> fmf = new FixedMaxFinder<>(nearest, (orgPoint, newPoint) -> {
					return (newPoint.getPosition().diff(this.getPosition()).abs()) > (orgPoint.getPosition().diff(this.getPosition()).abs());
				});
				for(DistanceAwarePoint p : points)
					fmf.add(p);
				toCalculate = fmf;
			} else {
				toCalculate = points;
			}
			
			for(DistanceAwarePoint p : toCalculate) {
				if(p != this && rng.nextDouble() >= gate) {
					double distance = p.getPosition().diff(this.getPosition()).abs();
					distance *= (1 + (-1 + rng.nextDouble()*2)*inaccuracy);
					target.put(p, distance );
				}
			}
		}
		
		public HashMap<DistanceAwarePoint, Double> getTargetDistances() {
			return targetDistances;
		}

		public HashMap<DistanceAwarePoint, Double> getDistances() {
			return distances;
		}
		
		public int getNumberOfReferences() {
			return targetDistances.size();
		}
	}
}
