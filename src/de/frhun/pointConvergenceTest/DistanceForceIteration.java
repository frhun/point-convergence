package de.frhun.pointConvergenceTest;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.function.DoubleFunction;
import java.util.function.IntFunction;

import de.frhun.pointConvergenceTest.DistanceAwarePoints.DistanceAwarePoint;

public class DistanceForceIteration {
	private double forceDistanceFactor = 1;
	private double dynamicGate = 0;
	
	private DistanceAwarePoints points;
	
	public DistanceForceIteration(List<? extends Point> points, boolean chain) {
		this.points = new DistanceAwarePoints(points, chain);
	}
	
	public DistanceForceIteration(List<? extends Point> points, double gate, int nearest) {
		this.points = new DistanceAwarePoints(points, gate, nearest);
	}
	
	public void calculateForces() {
		Random random = null;
		if(dynamicGate > 0)
			random = new Random();
		
		List<DistanceAwarePoint> pntli = points.getList();
		for(DistanceAwarePoint p : pntli) { //Parallelisierbar!
			p.caclulateDistances();
			Vector2D force = new Vector2D(0, 0);
			for(DistanceAwarePoint op : pntli) { //op = "other point"
				if(op != p && p.getTargetDistances().containsKey(op) && (random == null ? true : random.nextDouble() >= dynamicGate)) {
					double currentDistance = p.getDistances().get(op), targetDistance = p.getTargetDistances().get(op);
					double ratio;
					double jitter = 100;
					if(targetDistance == 0.0 || currentDistance == 0.0) {
						ratio = 1;
					}else {
						ratio = (currentDistance / targetDistance);
					}
					
					force.add(
							p.getPosition().diff(op.getPosition()).mul(
									1 - ratio
									)
							);
				}
			}
			p.setForce(force.mul(forceDistanceFactor/p.getNumberOfReferences()));
		}
	}
	
	public void iterate() {
		List<DistanceAwarePoint> pointlist = points.getList();
		for(DistanceAwarePoint p : pointlist) {
			p.setPosition(p.getPosition().add(p.getForce()));
		}
		calculateForces();
	}
	
	public void centerAll(Vector2D center) {
		double sideLength = 10, halfSideLength = sideLength / 2;
		double initialBoxDistributionDistance = 
				sideLength / ((points.getList().size()) / 4);
		int uniquePointsPerSide = points.getList().size() / 4;
		Iterator<DistanceAwarePoint> pointIterator = points.getList().iterator();
		for(int i = 0; i < uniquePointsPerSide; i++) {
			pointIterator.next().setPosition(center.clone().add(new Vector2D(-halfSideLength + i * initialBoxDistributionDistance, -halfSideLength)));
		}
		for(int i = 0; i < uniquePointsPerSide; i++) {
			pointIterator.next().setPosition(center.clone().add(new Vector2D(halfSideLength, -halfSideLength + i * initialBoxDistributionDistance)));
		}
		for(int i = 0; i < uniquePointsPerSide; i++) {
			pointIterator.next().setPosition(center.clone().add(new Vector2D(halfSideLength - i * initialBoxDistributionDistance, halfSideLength)));
		}
		for(int i = 0; i < uniquePointsPerSide; i++) {
			pointIterator.next().setPosition(center.clone().add(new Vector2D(-halfSideLength, halfSideLength - i * initialBoxDistributionDistance)));
		}
		while(pointIterator.hasNext()) {
			pointIterator.next().setPosition(center.clone());
		}
	}
	
	public PointsState getSnapshot() {
		return new PointsState(points.getList());
	}
	
	public void setDynamicGate(double gate) { this.dynamicGate = gate; }
	
	public void setForceDistanceFactor(double forceDistanceFactor) { this.forceDistanceFactor = forceDistanceFactor; }
}
