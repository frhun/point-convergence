package de.frhun.java.util;

import java.util.Iterator;

public class FixedMaxFinder<T> implements Iterable<T>{
	
	private int size = 0;
	private Element first = null;
	private Comparer<T> comparer;
	
	public FixedMaxFinder(int size, Comparer<T> comparer) throws IllegalArgumentException{
		if(size == 0)
			throw new IllegalArgumentException("Size must be at least 1.");
		this.size = size;
		if(comparer == null)
			throw new IllegalArgumentException("Comparer can't be null.");
		this.comparer = comparer;
	}
	
	public void add(T toAdd) {
		Element currentLargest = first;
		if(first == null) {
			first = new Element(toAdd, null, null);
			return;
		}
		
		if(comparer.isBigger(currentLargest.getContent(), toAdd)){
			first = new Element(toAdd, null, currentLargest);
			return;
		}
		
		while(!currentLargest.isLast()) {
			if(comparer.isBigger(currentLargest.getContent(), toAdd)){
				new Element(toAdd, currentLargest.getPrevious(), currentLargest);
				return;
			}
			currentLargest = currentLargest.getNext();
		};
		
		new Element(toAdd, currentLargest, null);
	}
	
	public int size() { return size; }
	
	private class Element{
		T content;
		Element previous;
		Element next;
		int index;
		
		public Element(T content, FixedMaxFinder<T>.Element previous, FixedMaxFinder<T>.Element next) {
			if(previous == null) {
				index = 0;
			}else {
				index = previous.getIndex() + 1;
				if(index >= size) {
					return;
				}
				previous.setNext(this);
			}
			this.content = content;
			this.previous = previous;
			this.next = next;
			if(next != null) {
				next.setPrevious(this);
				next.shove();
			}
		}
		
		public T getContent() {
			return content;
		}

		public Element getPrevious() {
			return previous;
		}

		public void setPrevious(Element previous) {
			this.previous = previous;
		}

		public Element getNext() {
			return next;
		}

		public void setNext(Element next) {
			this.next = next;
		}
		
		private boolean isLast() {
			return next == null;
		}
		
		private int getIndex() {
			return index;
		}
		
		private void shove() {
			if(++index >= size) {
				previous.setNext(null);
			}else if(next != null){
				next.shove();
			}
		}
	}
	
	public interface Comparer<T>{
		public boolean isBigger(T orignal, T newobj);
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			Element current = first;
			
			@Override
			public boolean hasNext() {
				return current != null;
			}

			@Override
			public T next() {
				T toReturn = current.getContent();
				current = current.getNext();
				return toReturn;
			}
			
		};
	}
}
