package de.frhun.pointConvergenceTest;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MainWindow extends JFrame {
	
	private Renderer renderer;
	private static double scaleFactorFactor = 1;
	
	private static ArrayList<PointsState> snapshots;
	
	public static void main(String[] args) throws InterruptedException {
		
		try {
			scaleFactorFactor = Double.valueOf(args[0]);
		}catch(ArrayIndexOutOfBoundsException|NumberFormatException e) {}
		
		uiselect:
        try {
            UIManager.LookAndFeelInfo[] feels = UIManager.getInstalledLookAndFeels();
            for(UIManager.LookAndFeelInfo feel : feels) {
                if(feel.getName().toLowerCase().contains("gtk")) {
                    UIManager.setLookAndFeel(feel.getClassName());
                    break uiselect;
                }
            }
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
//		deliberatePoints();
		randomPointGen();
	}
	
	public static void deliberatePoints() throws InterruptedException {
		ArrayList<Point> points = new ArrayList<>();
		points.add(new Point(25000, 10000));
		points.add(new Point(25000, 20000));
		points.add(new Point(25000, 30000));
		points.add(new Point(25000, 40000));
		Point p1 = new Point(10000, 25000);
		p1.setCat(1);
		points.add(p1);
		Point p2 = new Point(30000, 25000);
		p2.setCat(2);
		points.add(p2);
		
		MainWindow origWindow = new MainWindow();
		origWindow.setTitle("Original");
		
		MainWindow iterWindow = new MainWindow();
		iterWindow.setTitle("Simulation");

		DistanceForceIteration sim = new DistanceForceIteration(points, false);
		sim.calculateForces();
		origWindow.setPointsState(sim.getSnapshot());
		sim.centerAll(new Vector2D(25000, 25000));
		iterWindow.setPointsState(sim.getSnapshot());
		
		Thread.sleep(1000);
		for(int i = 0; i < 100; i++) {
			Thread.sleep(100);
			sim.iterate();
			iterWindow.setPointsState(sim.getSnapshot());
		}
	}
	
	private static double gate = 0;
	private static Consumer<Double> dynamicGate;
	private static double dynamicGateInitial = 0;
	private static Consumer<Double> forceDistanceFactor;
	private static double forceDistanceFactorInitial = 1;
	private static int delay = 0;
	private static int iterations = 300;
	
	public static void randomPointGen() {
		MainWindow simulationWindow = new MainWindow();
		MainWindow sourceWindow = new MainWindow();
		sourceWindow.setTitle("original");
		sourceWindow.setBounds(10, 100, sourceWindow.getWidth(), sourceWindow.getHeight());
		simulationWindow.setBounds(20 + sourceWindow.getWidth(), 100, simulationWindow.getWidth(), simulationWindow.getHeight());
		
		JFrame controlWindow = new JFrame("Steuerung");
		Container cwContentPane = controlWindow.getContentPane();
		cwContentPane.setLayout(new BoxLayout(cwContentPane, BoxLayout.Y_AXIS));
		
		JLabel startEnviromentControlsLabel = new JLabel("Starteinstellungen");
		Font boldFont = startEnviromentControlsLabel.getFont();
		boldFont = new Font(boldFont.getFontName(), Font.BOLD, (boldFont.getSize()*3)/2);
		startEnviromentControlsLabel.setFont(boldFont);
		cwContentPane.add(startEnviromentControlsLabel);
		
		JCheckBox chainCB = new JCheckBox("Ketten Entfernungen");
		cwContentPane.add(chainCB);
		
		cwContentPane.add(new JLabel("Anzahl Punkte"));
		JSlider nSlider = new JSlider(3, 700, 20);
		nSlider.setMinorTickSpacing(1);
		nSlider.setMajorTickSpacing(10);
		nSlider.setSnapToTicks(true);
		cwContentPane.add(nSlider);
		
		cwContentPane.add(new JLabel("Anzahl Bekannter (nächster nachbarn)"));
		JSlider nearestSlider = new JSlider(0, 50, 0);
		nearestSlider.setMinorTickSpacing(1);
		nearestSlider.setMajorTickSpacing(10);
		nearestSlider.setSnapToTicks(true);
		cwContentPane.add(nearestSlider);
		
		cwContentPane.add(new JLabel("±% Ungenauigkeit der Ausgangswerte"));
		JSlider inaccuracySlider = new JSlider(0, 100, 0);
		inaccuracySlider.createStandardLabels(1);
		cwContentPane.add(inaccuracySlider);
		inaccuracySlider.addChangeListener((e) -> {
			DistanceAwarePoints.inaccuracy = (double)inaccuracySlider.getValue()/100.0;
		});
		
		cwContentPane.add(new JLabel("Abstands unbestimmtheits Rate"));
		JSlider distanceUnconstrainedRateSlider = new JSlider(0, 100 , 0);
		distanceUnconstrainedRateSlider.addChangeListener((ChangeEvent e) ->{
			gate = (double)distanceUnconstrainedRateSlider.getValue()/100;
		});
		cwContentPane.add(distanceUnconstrainedRateSlider);
		
		chainCB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean enableDepends = !chainCB.isSelected();
				distanceUnconstrainedRateSlider.setEnabled(enableDepends);
				inaccuracySlider.setEnabled(enableDepends);
				nearestSlider.setEnabled(enableDepends);
			}
		});
		
		JCheckBox recordCB = new JCheckBox("Aufzeichnen");
		recordCB.setSelected(true);
		cwContentPane.add(recordCB);
		
		JLabel runtimeControlsLabel = new JLabel("Laufzeiteinstellungen");
		runtimeControlsLabel.setFont(boldFont);
		cwContentPane.add(runtimeControlsLabel);
		
		cwContentPane.add(new JLabel("dynamische Abstands unbestimmtheits Rate"));
		JSlider dynamicDistanceUnconstrainedRateSlider = new JSlider(0, 100 , 0);
		dynamicDistanceUnconstrainedRateSlider.addChangeListener((ChangeEvent e) ->{
			dynamicGateInitial = dynamicDistanceUnconstrainedRateSlider.getValue()/100.0;
			if(dynamicGate != null)
				dynamicGate.accept(dynamicGateInitial);
		});
		cwContentPane.add(dynamicDistanceUnconstrainedRateSlider);
		
		cwContentPane.add(new JLabel("Kraft entfernungs Faktor"));
		JSlider forceDistanceFactorSlider = new JSlider(1, 100 , 10);
		forceDistanceFactorSlider.addChangeListener((ChangeEvent e) ->{
			forceDistanceFactorInitial = forceDistanceFactorSlider.getValue()/10.0;
			if(forceDistanceFactor != null)
				forceDistanceFactor.accept(forceDistanceFactorInitial);
		});
		cwContentPane.add(forceDistanceFactorSlider);
		
		cwContentPane.add(new JLabel("Anzahl der simulations iterationen"));
		JSpinner iterationsSpinner = new JSpinner(new SpinnerNumberModel(iterations, 1, 10000000, 100));
		iterationsSpinner.addChangeListener((ChangeEvent e) ->{
			int val = (int)iterationsSpinner.getValue();
			iterations = Integer.valueOf(val);
			boolean enableRecord = val < 50000;
			recordCB.setSelected(enableRecord);
			recordCB.setEnabled(enableRecord);
		});
		cwContentPane.add(iterationsSpinner);
		
		JButton startButton = new JButton("START");
		cwContentPane.add(startButton);
		
		cwContentPane.add(new JLabel("Verlangsamung (ms Delay pro iteration)"));
		JSlider delaySlider = new JSlider(0, 1000, 0);
		delaySlider.createStandardLabels(1);
		cwContentPane.add(delaySlider);
		delaySlider.addChangeListener((e) -> {
			delay = delaySlider.getValue();
		});
		
		cwContentPane.add(new JLabel("Frame der Aufzeichnung"));
		JSlider frameSelectionSlider = new JSlider(0,0,0);
		frameSelectionSlider.setSnapToTicks(true);
		frameSelectionSlider.setEnabled(false);
		cwContentPane.add(frameSelectionSlider);
		
		JToggleButton xInvButton = new JToggleButton("Invert X");
		JToggleButton yInvButton = new JToggleButton("Invert Y");
		ActionListener invListender = (e) ->{
			simulationWindow.setInvertedX(xInvButton.isSelected());
			simulationWindow.setInvertedY(yInvButton.isSelected());
		};
		xInvButton.addActionListener(invListender);
		yInvButton.addActionListener(invListender);
		cwContentPane.add(xInvButton);
		cwContentPane.add(yInvButton);
		
		JCheckBox centerOnWeightCB = new JCheckBox("auf Schwerpunkt zentrieren");
		centerOnWeightCB.addActionListener((e) ->{
			simulationWindow.setCenterOnWeight(centerOnWeightCB.isSelected());
		});
		cwContentPane.add(centerOnWeightCB);
		
		JCheckBox drawConnectionsCB = new JCheckBox("Verbindungsstrahlen zeichnen");
		drawConnectionsCB.addActionListener((e) ->{
			simulationWindow.setDrawConnections(drawConnectionsCB.isSelected());
			sourceWindow.setDrawConnections(drawConnectionsCB.isSelected());
			sourceWindow.repaint();
		});
		cwContentPane.add(drawConnectionsCB);
		
		controlWindow.setDefaultCloseOperation(EXIT_ON_CLOSE);
		controlWindow.setMinimumSize(new Dimension(100, 100));
		controlWindow.pack();
		
		startButton.addActionListener(new ActionListener() {
			Thread currentThread = null;
			@Override
			public void actionPerformed(ActionEvent e) {
				if(currentThread != null) {
					currentThread.interrupt();
					try {
						currentThread.join();
					} catch (InterruptedException e1) {
						System.out.println("interrupted");
					}
				}
				currentThread = new Thread(()-> {
					boolean record = recordCB.isSelected();
					snapshots = (record ? new ArrayList<>(300) : null);
					LinkedList<Point> points = new LinkedList<>();
					{
						Random rng = new Random();
						for(int i = 0; i < nSlider.getValue(); i++) {
							Point p = new Point(rng.nextInt(30000) + 10000, rng.nextInt(30000)+10000);
							if(i%3 == 0) {
								p.setCat(1);
							}
							if(i%6 == 0) {
								p.setCat(2);
							}
							if(i%12 == 0) {
								p.setCat(3);
							}
							points.add(p);
						}
					}
					DistanceForceIteration dfi;
					if(chainCB.isSelected()) {
						dfi = new DistanceForceIteration(points, true);
					} else {
						dfi = new DistanceForceIteration(points, gate, nearestSlider.getValue());
					}
					dynamicGate = (Double gate) -> {
						dfi.setDynamicGate(gate);
					};
					forceDistanceFactor = (Double fdf) -> {
						dfi.setForceDistanceFactor(fdf);
					};
					dfi.setDynamicGate(dynamicGateInitial);
					dfi.setForceDistanceFactor(forceDistanceFactorInitial);
					sourceWindow.setPointsState(dfi.getSnapshot());
					dfi.centerAll(new Vector2D(25000, 25000));
					dfi.calculateForces();
					simulationWindow.setPointsState(dfi.getSnapshot());

					frameSelectionSlider.setEnabled(false);
					if(record)
						snapshots.add(dfi.getSnapshot());
					long lastSnap = 0;
					for(int i = 1; i < iterations; i++) {
						dfi.iterate();
						PointsState ps = null;
						long currentMillis = System.currentTimeMillis();
						if(currentMillis - lastSnap > 5) {
							simulationWindow.setTitle("Iteration: " + i);
							ps = dfi.getSnapshot();
							simulationWindow.setPointsState(ps);
							lastSnap = currentMillis;
						}
						if(record) {
							if(ps == null)
								ps = dfi.getSnapshot();
							snapshots.add(ps);
							frameSelectionSlider.setMaximum(i);
							frameSelectionSlider.setValue(i);
						}
						try {
							Thread.sleep(delay);
						} catch (InterruptedException e1) {
							break;
						}
						if(Thread.interrupted()) {
							break;
						}
					}
					simulationWindow.setTitle("Iteration: " + (iterations - 1));
					simulationWindow.setPointsState(dfi.getSnapshot());
					frameSelectionSlider.setEnabled(record);
				});
				Runtime.getRuntime().gc();
				currentThread.start();
			}
		});
		
		frameSelectionSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				simulationWindow.setPointsState(snapshots.get(frameSelectionSlider.getValue()));
				simulationWindow.setTitle("Iteration: " + frameSelectionSlider.getValue());
			}
		});

		controlWindow.setAlwaysOnTop(true);
		Rectangle controlPosition = (Rectangle)sourceWindow.getBounds().clone();
		controlPosition.x += controlPosition.width - controlWindow.getWidth();
		controlPosition.y += controlPosition.height - controlWindow.getHeight();
		controlPosition.height = controlWindow.getHeight();
		controlPosition.width = controlWindow.getWidth();
		controlWindow.setBounds(controlPosition);
		controlWindow.setVisible(true);
	}
	
	public MainWindow() {
		super();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setTitle("Point Convergence Test");
		Container contentPane = this.getContentPane();
		renderer = new Renderer(1.8 * scaleFactorFactor);
		contentPane.add(renderer);
		this.setMinimumSize(renderer.getSize());
		this.pack();
		this.setVisible(true);
	}
	
	public void setState(List<? extends Point> points) {
		renderer.setCurrentState(new PointsState(points));
		renderer.repaint();
	}
	
	public void setPointsState(PointsState ps) {
		renderer.setCurrentState(ps);
		renderer.repaint();
	}
	
	public void setInvertedX(boolean inverted) {
		renderer.getArranger().setInvertedX(inverted);
	}
	
	public void setInvertedY(boolean inverted) {
		renderer.getArranger().setInvertedY(inverted);
	}
	
	public void setCenterOnWeight(boolean centerOnWeight) {
		renderer.centerOnWeight = centerOnWeight;
	}
	
	public void setDrawConnections(boolean drawConnections) {
		renderer.drawConnections = drawConnections;
	}
}
