package de.frhun.java.util;

import java.util.Arrays;
import java.util.LinkedList;

public class tests {
	public static void main(String[] args) {
		FixedMaxFinder<Integer> mf = new FixedMaxFinder<>(3, (Integer io, Integer in) -> {
			return io < in;
		});
		
		LinkedList<Integer> numbers = new LinkedList<>();
		numbers.addAll( Arrays.asList(8,4,3,2,-1,5,7) );
		for(int i : numbers) {
			mf.add(i);
		}
		for(int i : mf) {
			System.out.println(i);
		}
	}
}
