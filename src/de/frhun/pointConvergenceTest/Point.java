package de.frhun.pointConvergenceTest;

public class Point implements java.lang.Cloneable {
	private Vector2D position;
	private Vector2D force;
	private static int serialNo;
	private String name = Integer.valueOf(serialNo++).toString();

	private int category = 0;
	
	public Point(Vector2D pos) {
		this.position = pos;
	}
	
	public Point(int x, int y) {
		position = new Vector2D(x, y);
	}
	
	public Point() {
		position = new Vector2D();
	}
	
	public boolean hasForce() {
		return force != null;
	}
	
	public Vector2D getPosition() { return position.clone(); }
	public void setPosition(Vector2D position) { this.position = position.clone(); }

	public Vector2D getForce() { return force.clone(); }
	public void setForce(Vector2D force) { this.force = force.clone(); }

	public int getCat() { return category; }
	public void setCat(int cat) { this.category = cat; }
	
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	
	public String toString() { return getName(); }
	
	public Point clone() {
		Point p = new Point(position.clone());
		if(force != null)
			p.setForce(force.clone());
		p.setCat(category);
		return p;
	}
}
